package lang.sep3asm.parse;

import lang.FatalErrorException;
import lang.sep3asm.Sep3asmParseContext;
import lang.sep3asm.Sep3asmParseRule;
import lang.sep3asm.Sep3asmToken;
import lang.sep3asm.Sep3asmTokenizer;
import lang.sep3asm.instruction.Sep3Instruction;

public class Inst1 extends Sep3asmParseRule {
    // inst1 ::= INST1 operand COMMA operand
    private Sep3asmToken inst;
    private Operand op1, op2;
    private Sep3Instruction sep3inst;

    public Inst1(Sep3asmParseContext ctx) {
    }

    public static boolean isFirst(Sep3asmToken tk) {
        return tk.getType() == Sep3asmToken.TK_INST1;
    }

    public void parse(Sep3asmParseContext ctx) throws FatalErrorException {
        Sep3asmTokenizer ct = ctx.getTokenizer();
        inst = ct.getCurrentToken(ctx);
        Sep3asmToken tk = ct.getNextToken(ctx);
        if (Operand.isFirst(tk)) {
            op1 = new Operand(ctx);
            op1.parse(ctx);
        } else {
            ctx.warning(tk.toExplainString() + "オペランドが来ます");
        }
    }
    public void pass1(Sep3asmParseContext ctx) throws FatalErrorException {
        sep3inst = ctx.getTokenizer().getInstruction(inst.getText(), ctx);
        if (op1 != null) {
            op1.pass1(ctx);
            op1.limit(sep3inst.getOp1Info(), ctx, inst, "fromオペランドとして");
            if (op1.needExtraWord()) { ctx.addLocationCounter(2); }
            else					 { ctx.addLocationCounter(1); }
        }
    }
    public void pass2(Sep3asmParseContext ctx) throws FatalErrorException {
        if (op1 != null) { op1.pass2(ctx); }
        if (op2 != null) { op2.pass2(ctx); }
        sep3inst.generate(ctx, op1, op2);
    }
}
