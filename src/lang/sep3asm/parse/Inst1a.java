package lang.sep3asm.parse;

import lang.FatalErrorException;
import lang.sep3asm.Sep3asmParseContext;
import lang.sep3asm.Sep3asmParseRule;
import lang.sep3asm.Sep3asmToken;
import lang.sep3asm.Sep3asmTokenizer;
import lang.sep3asm.instruction.Sep3Instruction;

public class Inst1a extends Sep3asmParseRule {
    // inst1a ::= INST1a operand COMMA operand
    private Sep3asmToken inst;
    private Operand op1, op2;
    private Sep3Instruction sep3inst;

    public Inst1a(Sep3asmParseContext ctx) {
    }

    public static boolean isFirst(Sep3asmToken tk) {
        return tk.getType() == Sep3asmToken.TK_INST1a;
    }

    public void parse(Sep3asmParseContext ctx) throws FatalErrorException {
        Sep3asmTokenizer ct = ctx.getTokenizer();
        inst = ct.getCurrentToken(ctx);
        Sep3asmToken tk = ct.getNextToken(ctx);
        if (Operand.isFirst(tk)) {
            op2 = new Operand(ctx);
            op2.parse(ctx);
        } else {
            ctx.warning(tk.toExplainString() + "オペランドが来ます");
        }
    }
    public void pass1(Sep3asmParseContext ctx) throws FatalErrorException {
        sep3inst = ctx.getTokenizer().getInstruction(inst.getText(), ctx);
        if (op2 != null) {
            op2.pass1(ctx);
            op2.limit(sep3inst.getOp2Info(), ctx, inst, "toオペランドとして");
            ctx.addLocationCounter(1);
        }
    }
    public void pass2(Sep3asmParseContext ctx) throws FatalErrorException {
        if (op1 != null) { op1.pass2(ctx); }
        if (op2 != null) { op2.pass2(ctx); }
        sep3inst.generate(ctx, op1, op2);
    }
}
