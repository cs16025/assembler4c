package lang.sep3asm.parse;

import lang.FatalErrorException;
import lang.sep3asm.*;

public class LabeledLine extends Sep3asmParseRule {
    //LabeledLine ::= Label COLON | Label EQUAL NUM | Label EQUAL Label
    private String label1;
    private NumOrIdent noi;
    private int type;

    public static final int LC = 0;
    public static final int LE = 1;

    public LabeledLine(Sep3asmParseContext ctx) {
        label1 = null;
        noi = null;
        type = -1;
    }

    public static boolean isFirst(Sep3asmToken tk) {
        return tk.getType() == Sep3asmToken.TK_IDENT;
    }

    public void parse(Sep3asmParseContext ctx) throws FatalErrorException {
        Sep3asmTokenizer ct = ctx.getTokenizer();
        Sep3asmToken tk = ct.getCurrentToken(ctx);
        if (tk.getType() == Sep3asmToken.TK_IDENT){
            label1 = tk.getText();
            tk = ct.getNextToken(ctx);
            if (tk.getType() == Sep3asmToken.TK_COLON) {
                type = LC;
                tk = ct.getNextToken(ctx);
            } else if (tk.getType() == Sep3asmToken.TK_EQUAL) {
                tk = ct.getNextToken(ctx);
                if (NumOrIdent.isFirst(tk)) {
                    NumOrIdent noi = new NumOrIdent(ctx);
                    noi.parse(ctx);
                    this.noi = noi;
                    type = LE;
                } else {
                    ctx.warning(tk.toExplainString() + "ラベル定義行が不正です");
                }
            } else {
                ctx.warning(tk.toExplainString() + "ラベル定義行が不正です");
            }
        } else {
            ctx.warning(tk.toExplainString() + "ラベル定義行が不正です");
        }
    }

    public void pass1(Sep3asmParseContext ctx) throws FatalErrorException {
        Sep3asmSymbolTable st = ctx.getSymbolTable();
        LabelEntry le = new LabelEntry();
        if (type == LC) {
            le.setInteger(ctx.getLocationCounter());
        } else {
            Integer num;
            try {
                num = Integer.decode(noi.getString());
                le.setInteger(num);
            } catch (NumberFormatException e) {
                le.setLabel(noi.getString());
            }
        }
        st.register(label1, le);
    }

    public void pass2(Sep3asmParseContext pcx) throws FatalErrorException {
    }
}
