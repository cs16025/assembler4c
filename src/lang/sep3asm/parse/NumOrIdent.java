package lang.sep3asm.parse;

import lang.FatalErrorException;
import lang.sep3asm.Sep3asmParseContext;
import lang.sep3asm.Sep3asmParseRule;
import lang.sep3asm.Sep3asmToken;
import lang.sep3asm.Sep3asmTokenizer;

public class NumOrIdent extends Sep3asmParseRule {
    // NumOrIdent ::= NUM || IDENT
    private String noi;

    public NumOrIdent(Sep3asmParseContext ctx) {
        noi = null;
    }

    public static boolean isFirst(Sep3asmToken tk) {
        return tk.getType() == Sep3asmToken.TK_NUM || tk.getType() == Sep3asmToken.TK_IDENT;
    }

    public String getString() {
        return noi;
    }

    public Integer getNumorIdent(Sep3asmParseContext ctx) {
        Integer numbuf;
        try {
            numbuf = Integer.decode(noi);
        } catch (NumberFormatException e) {
            numbuf = ctx.getSymbolTable().resolve(noi);
        }
        return numbuf;
    }

    public void parse(Sep3asmParseContext ctx) throws FatalErrorException {
        Sep3asmTokenizer ct = ctx.getTokenizer();
        Sep3asmToken tk = ct.getCurrentToken(ctx);
        if (tk.getType() == Sep3asmToken.TK_NUM || tk.getType() == Sep3asmToken.TK_IDENT) {
            noi = tk.getText();
            tk = ct.getNextToken(ctx);
        } else {
            ctx.warning(tk.toExplainString() + "不正な識別子または数値の指定です");
        }
    }

    public void pass1(Sep3asmParseContext ctx) throws FatalErrorException {
    }

    public void pass2(Sep3asmParseContext ctx) throws FatalErrorException {
    }
}
