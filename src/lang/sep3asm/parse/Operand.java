package lang.sep3asm.parse;

import javafx.geometry.Pos;
import lang.*;
import lang.sep3asm.*;

public class Operand extends Sep3asmParseRule {
    // operand ::= REG | LPAR REG RPAR | LPAR REG RPAR [PLUS] | MINUS LPAR REG RPAR | SHARP NumOrIdent | NumOrIdent
    public static final int REGISTER	= 001;
    public static final int INDIRECT	= 002;
    public static final int PREDEC		= 004;
    public static final int POSTINC		= 010;
    public static final int IMM			= 020;
    public static final int LABEL		= 040;

    private int opMode;
    private  Sep3asmToken reg;
    private int opExt;
    private NumOrIdent noi;

    public Operand(Sep3asmParseContext ctx) {
        opMode = 0;
        reg = null;
        opExt = 0;
        noi = null;
    }
    public static boolean isFirst(Sep3asmToken tk) {
        int type = tk.getType();
        return type == Sep3asmToken.TK_REG || type == Sep3asmToken.TK_LBRACKET || type == Sep3asmToken.TK_MINUS || type == Sep3asmToken.TK_SHARP || type == Sep3asmToken.TK_IDENT;
    }
    public void parse(Sep3asmParseContext ctx) throws FatalErrorException {
        Sep3asmTokenizer ct = ctx.getTokenizer();
        Sep3asmToken tk = ct.getCurrentToken(ctx);
        int type = tk.getType();
        if (type == Sep3asmToken.TK_REG) {
            opMode = REGISTER;
            reg = tk;
            tk = ct.getNextToken(ctx);
        } else if (type == Sep3asmToken.TK_LBRACKET) {
            tk = ct.getNextToken(ctx);
            type = tk.getType();
            if (type == Sep3asmToken.TK_REG) {
                reg = tk;
                tk = ct.getNextToken(ctx);
                type = tk.getType();
                if (type == Sep3asmToken.TK_RBRACKET) {
                    opMode = INDIRECT;
                    tk = ct.getNextToken(ctx);
                    type = tk.getType();
                    if (type == Sep3asmToken.TK_PLUS) {
                        opMode = POSTINC;
                        tk = ct.getNextToken(ctx);
                    }
                } else {
                    ctx.warning(tk.toExplainString() + "不正なオペランド指定です");
                }
            } else {
                ctx.warning(tk.toExplainString() + "不正なオペランド指定です");
            }
        } else if (type == Sep3asmToken.TK_MINUS) {
            tk = ct.getNextToken(ctx);
            type = tk.getType();
            if (type == Sep3asmToken.TK_LBRACKET) {
                tk = ct.getNextToken(ctx);
                type = tk.getType();
                if (type == Sep3asmToken.TK_REG) {
                    reg = tk;
                    tk = ct.getNextToken(ctx);
                    type = tk.getType();
                    if (type == Sep3asmToken.TK_RBRACKET) {
                        opMode = PREDEC;
                        tk = ct.getNextToken(ctx);
                    } else {
                        ctx.warning(tk.toExplainString() + "不正なオペランド指定です");
                    }
                } else {
                    ctx.warning(tk.toExplainString() + "不正なオペランド指定です");
                }
            } else {
                ctx.warning(tk.toExplainString() + "不正なオペランド指定です");
            }
        } else if (type == Sep3asmToken.TK_SHARP) {
            tk = ct.getNextToken(ctx);
            if (NumOrIdent.isFirst(tk)) {
                opMode = IMM;
                NumOrIdent noi = new NumOrIdent(ctx);
                noi.parse(ctx);
                this.noi = noi;
            } else {
                ctx.warning(tk.toExplainString() + "不正なオペランド指定です");
            }
        } else {
            tk = ct.getCurrentToken(ctx);
            if (NumOrIdent.isFirst(tk)) {
                opMode = LABEL;
                NumOrIdent noi = new NumOrIdent(ctx);
                noi.parse(ctx);
                this.noi = noi;
            } else {
                ctx.warning(tk.toExplainString() + "不正なオペランド指定です");
            }
        }
    }

    private int fivebits;
    private boolean needExtraWord;
    private int extraWord;

    public boolean needExtraWord() { return needExtraWord; }
    public int to5bits() {
        if (opMode == REGISTER) {
            fivebits = 0x0;
            fivebits |= opExt;
        } else if (opMode == INDIRECT) {
            fivebits = 0x8;
            fivebits |= opExt;
        } else if (opMode == PREDEC) {
            fivebits = 0x10;
            fivebits |= opExt;
        } else if (opMode == POSTINC) {
            fivebits = 0x18;
            fivebits |= opExt;
        } else if (opMode == IMM || opMode == LABEL) {
            fivebits = 0x1f;
        }
        return fivebits;
    }
    public int getExtraWord() {
        extraWord = 0;
        if (needExtraWord) {
            extraWord = opExt;
        }
        return extraWord;
    }

    public void pass1(Sep3asmParseContext ctx) throws FatalErrorException {
        if (opMode == REGISTER || opMode == INDIRECT || opMode == PREDEC || opMode == POSTINC) {
            needExtraWord =  false;
        } else if (opMode == IMM || opMode == LABEL) {
            needExtraWord = true;
        }
    }
    public void limit(int info, Sep3asmParseContext ctx, Sep3asmToken inst, final String s) {
        if (s.compareTo("fromオペランドとして") == 0) {
            if (opMode == PREDEC && reg.getRegisterNumber() != 6) {
                ctx.warning("プレデクリメントモードはR6(SP)以外使用できません");
            }
        } else if (s.compareTo("toオペランドとして") == 0) {
            if ((opMode == INDIRECT || opMode == POSTINC) && reg.getRegisterNumber() == 7) {
                ctx.warning("TOオペランドでレジスタ間接アドレッシングモードはR7に使用できません");
            }
        }
        if ((opMode & info) == 0) {
            ctx.warning("命令とアドレッシングモードの指定が正しくありません");
        }
    }
    public void pass2(Sep3asmParseContext ctx) throws FatalErrorException {
        if (opMode == IMM || opMode == LABEL) {
            opExt = noi.getNumorIdent(ctx);
        } else {
            opExt = reg.getRegisterNumber();
        }
    }
}
