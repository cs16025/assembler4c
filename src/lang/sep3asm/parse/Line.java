package lang.sep3asm.parse;

import lang.*;
import lang.sep3asm.*;

public class Line extends Sep3asmParseRule {
    // line ::= NL | labeledLine NL | instLine NL | defLine NL
    private Sep3asmParseRule line;

    public Line(Sep3asmParseContext ctx) {
        line = null;
    }

    public static boolean isFirst(Sep3asmToken tk) {
        return LabeledLine.isFirst(tk) || InstLine.isFirst(tk) || DefLine.isFirst(tk) || tk.getType() == Sep3asmToken.TK_NL;
    }

    public void parse(Sep3asmParseContext ctx) throws FatalErrorException {
        Sep3asmTokenizer ct = ctx.getTokenizer();
        Sep3asmToken tk = ct.getCurrentToken(ctx);
        if (LabeledLine.isFirst(tk)) {
            Sep3asmParseRule labeledLine = new LabeledLine(ctx);
            labeledLine.parse(ctx);
            line = labeledLine;
        } else if (InstLine.isFirst(tk)) {
            Sep3asmParseRule instLine = new InstLine(ctx);
            instLine.parse(ctx);
            line = instLine;
        } else if (DefLine.isFirst(tk)) {
            Sep3asmParseRule defLine = new DefLine(ctx);
            defLine.parse(ctx);
            line = defLine;
        }
        tk = ct.getCurrentToken(ctx);
        if (tk.getType() == Sep3asmToken.TK_NL) {
            tk = ct.getNextToken(ctx);
        } else if (tk.getType() == Sep3asmToken.TK_EOF) {
        } else {
            ctx.warning(tk.toExplainString() + "余分なものがあります");
            tk = ct.getNextToken(ctx);
            while (tk.getType() != Sep3asmToken.TK_NL) {
                tk = ct.getNextToken(ctx);
            }
            tk = ct.getNextToken(ctx);
        }
    }
    public void pass1(Sep3asmParseContext ctx) throws FatalErrorException {
        if (line != null) {
            line.pass1(ctx);
        }
    }
    public void pass2(Sep3asmParseContext ctx) throws FatalErrorException {
        if (line != null) {
            line.pass2(ctx);
        }
    }
}
