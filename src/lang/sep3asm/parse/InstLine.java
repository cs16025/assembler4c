package lang.sep3asm.parse;

import lang.FatalErrorException;
import lang.sep3asm.*;

public class InstLine extends Sep3asmParseRule {
    //instLine ::= inst0 | inst1 | inst1a | inst2
    private Sep3asmParseRule iLine;

    public InstLine(Sep3asmParseContext ctx) {
        iLine = null;
    }

    public static boolean isFirst(Sep3asmToken tk) {
        return Inst0.isFirst(tk) || Inst1.isFirst(tk) || Inst1a.isFirst(tk) || Inst2.isFirst(tk);
    }

    public void parse(Sep3asmParseContext ctx) throws FatalErrorException {
        Sep3asmTokenizer ct = ctx.getTokenizer();
        Sep3asmToken tk = ct.getCurrentToken(ctx);
        int type = tk.getType();
        if (type == Sep3asmToken.TK_INST0) {
            Sep3asmParseRule inst0 = new Inst0(ctx);
            inst0.parse(ctx);
            iLine = inst0;
        } else if (type == Sep3asmToken.TK_INST1) {
            Sep3asmParseRule inst1 = new Inst1(ctx);
            inst1.parse(ctx);
            iLine = inst1;
        } else if (type == Sep3asmToken.TK_INST1a) {
            Sep3asmParseRule inst1a = new Inst1a(ctx);
            inst1a.parse(ctx);
            iLine = inst1a;
        } else if (type == Sep3asmToken.TK_INST2) {
            Sep3asmParseRule inst2 = new Inst2(ctx);
            inst2.parse(ctx);
            iLine = inst2;
        }
    }

    public void pass1(Sep3asmParseContext ctx) throws FatalErrorException {
        if (iLine != null) {
            iLine.pass1(ctx);
        }
    }

    public void pass2(Sep3asmParseContext ctx) throws FatalErrorException {
        if (iLine != null) {
            iLine.pass2(ctx);
        }
    }
}
