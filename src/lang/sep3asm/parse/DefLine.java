package lang.sep3asm.parse;

import lang.FatalErrorException;
import lang.sep3asm.Sep3asmParseContext;
import lang.sep3asm.Sep3asmParseRule;
import lang.sep3asm.Sep3asmToken;
import lang.sep3asm.Sep3asmTokenizer;

import java.util.ArrayList;

public class DefLine extends Sep3asmParseRule {
    //defLine ::= DOTBLKW NumOrIdent {, NumOrIdent} | DOTEND | DOTEQUAL NUM | DOTWD NumOrIdent {, NumOrIdent}
    private int type;
    private int num;
    private ArrayList<NumOrIdent> noiList;

    public DefLine(Sep3asmParseContext ctx) {
        type = -3;
        num = -1;
        noiList = new ArrayList<>();
    }

    public static boolean isFirst(Sep3asmToken tk) {
        int type = tk.getType();
        return type == Sep3asmToken.TK_DOTBLKW || type == Sep3asmToken.TK_DOTEND || type == Sep3asmToken.TK_DOT || type == Sep3asmToken.TK_DOTEQUAL || type == Sep3asmToken.TK_DOTWD;
    }

    public void parse(Sep3asmParseContext ctx) throws FatalErrorException {
        Sep3asmTokenizer ct = ctx.getTokenizer();
        Sep3asmToken tk = ct.getCurrentToken(ctx);
        int type = tk.getType();
        if (type == Sep3asmToken.TK_DOTBLKW) {
            this.type = Sep3asmToken.TK_DOTBLKW;
            tk = ct.getNextToken(ctx);
            if (NumOrIdent.isFirst(tk)) {
                NumOrIdent noi = new NumOrIdent(ctx);
                noi.parse(ctx);
                noiList.add(noi);
            } else {
                ctx.warning(tk.toExplainString() + "疑似命令が不正です");
            }
            tk = ct.getCurrentToken(ctx);
            type = tk.getType();
            while (type == Sep3asmToken.TK_COMMA) {
                tk = ct.getNextToken(ctx);
                if (NumOrIdent.isFirst(tk)) {
                    NumOrIdent noi = new NumOrIdent(ctx);
                    noi.parse(ctx);
                    noiList.add(noi);
                    tk = ct.getCurrentToken(ctx);
                    type = tk.getType();
                } else {
                    ctx.warning(tk.toExplainString() + "疑似命令が不正です");
                }
            }
        } else if (type == Sep3asmToken.TK_DOTEND) {
            System.out.println("「.END」を読み取りました。");
            this.type = Sep3asmToken.TK_DOTEND;
            tk = ct.getNextToken(ctx);
        } else if (type == Sep3asmToken.TK_DOT) {
            tk = ct.getNextToken(ctx);
            type = tk.getType();
            if (type == Sep3asmToken.TK_EQUAL) {
                this.type = Sep3asmToken.TK_DOTEQUAL;
                tk = ct.getNextToken(ctx);
                type = tk.getType();
                if (type == Sep3asmToken.TK_NUM) {
                    num = tk.getIntValue();
                    tk = ct.getNextToken(ctx);
                } else {
                    ctx.warning(tk.toExplainString() + "疑似命令が不正です");
                }
            } else {
                ctx.warning(tk.toExplainString() + "疑似命令が不正です");
            }
        } else if (type == Sep3asmToken.TK_DOTEQUAL) {
            this.type = Sep3asmToken.TK_DOTEQUAL;
            tk = ct.getNextToken(ctx);
            if (type == Sep3asmToken.TK_NUM) {
                num = tk.getIntValue();
                tk = ct.getNextToken(ctx);
            } else {
                ctx.warning(tk.toExplainString() + "疑似命令が不正です");
            }
        } else if (type == Sep3asmToken.TK_DOTWD) {
            this.type = Sep3asmToken.TK_DOTWD;
            tk = ct.getNextToken(ctx);
            if (NumOrIdent.isFirst(tk)) {
                NumOrIdent noi = new NumOrIdent(ctx);
                noi.parse(ctx);
                noiList.add(noi);
            } else {
                ctx.warning(tk.toExplainString() + "疑似命令が不正です");
            }
            tk = ct.getCurrentToken(ctx);
            type = tk.getType();
            while (type == Sep3asmToken.TK_COMMA) {
                tk = ct.getNextToken(ctx);
                if (NumOrIdent.isFirst(tk)) {
                    NumOrIdent noi = new NumOrIdent(ctx);
                    noi.parse(ctx);
                    noiList.add(noi);
                    tk = ct.getCurrentToken(ctx);
                    type = tk.getType();
                } else {
                    ctx.warning(tk.toExplainString() + "疑似命令が不正です");
                }
            }
        } else {
            ctx.warning(tk.toExplainString() + "疑似命令が不正です");
        }
    }

    public void pass1(Sep3asmParseContext ctx) throws FatalErrorException {
        if (type == Sep3asmToken.TK_DOTBLKW) {
            Integer numbuf;
            for (NumOrIdent noi : noiList) {
                numbuf = noi.getNumorIdent(ctx);
                if (numbuf == null) {
                    ctx.fatalError("ラベルを解決できません。終了します");
                }
                ctx.addLocationCounter(numbuf);
            }
        } else if (type == Sep3asmToken.TK_DOTEND) {
        } else if (type == Sep3asmToken.TK_DOTEQUAL) {
            ctx.setLocationCounter(num);
        } else if (type == Sep3asmToken.TK_DOTWD) {
            ctx.addLocationCounter(noiList.size());
        }
    }

    public void pass2(Sep3asmParseContext ctx) throws FatalErrorException {
        if (type == Sep3asmToken.TK_DOTBLKW) {
            Integer numbuf;
            for (NumOrIdent noi : noiList) {
                numbuf = noi.getNumorIdent(ctx);
                if (numbuf == null) {
                    ctx.fatalError("ラベルを解決できません。終了します");
                }
                ctx.addLocationCounter(numbuf);
            }
        } else if (type == Sep3asmToken.TK_DOTEND) {
        } else if (type == Sep3asmToken.TK_DOTEQUAL) {
            ctx.setLocationCounter(num);
        } else if (type == Sep3asmToken.TK_DOTWD) {
            Integer numbuf;
            for (NumOrIdent noi : noiList) {
                numbuf = noi.getNumorIdent(ctx);
                if (numbuf == null) {
                    ctx.fatalError("ラベルを解決できません。終了します");
                }
                ctx.getIOContext().getOutStream().printf("%08X:%04X\n",ctx.getLocationCounter(), numbuf);
                ctx.addLocationCounter(1);
            }
        }
    }
}
