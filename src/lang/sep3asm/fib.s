;;; program starts
	. = 0x100
	JMP	__START	; ProgramNode: 最初の実行文へ
;;; declaration starts
;;; intDecl starts
;;; declItem starts
a:
 .WORD 0	; DeclItem: 変数用の領域を確保<[3行目,5文字目の'a']>
;;; declItem completes
;;; declItem starts
b:
 .WORD 0	; DeclItem: 変数用の領域を確保<[3行目,9文字目の'b']>
;;; declItem completes
;;; declItem starts
c:
 .BLKW 10	; DeclItem: 配列用の領域を確保<[3行目,12文字目の'c']>
;;; declItem completes
;;; declItem starts
d:
 .BLKW 10	; DeclItem: 配列用の領域を確保<[3行目,20文字目の'd']>
;;; declItem completes
;;; intDecl completes
;;; declaration completes
;;; declaration starts
;;; constDecl starts
;;; constItem starts
e:
 .WORD 10	; ConstItem: 定数用の領域を確保<[4行目,11文字目の'e']>
;;; constItem completes
;;; constDecl completes
;;; declaration completes
__START:
	MOV	#0x1000, R6	; ProgramNode: 計算用スタック初期化
;;; statement starts
;;; statementAssign starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#a, (R6)+	; Ident: 変数アドレスを積む<[24行目,1文字目の'a']>
;;; ident completes
;;; variable completes
;;; Primary completes
;;; expression starts
;;; term starts
;;; factor starts
;;; unsignedFactor starts
;;; addressToValue starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#e, (R6)+	; Ident: 変数アドレスを積む<[24行目,5文字目の'e']>
;;; ident completes
;;; variable completes
;;; Primary completes
	MOV	-(R6), R0	; AddressToValue: アドレスを取り出して、内容を参照して、積む<>
	MOV	(R0), (R6)+	; AddressToValue:
;;; addressToValue completes
;;; unsignedFactor completes
;;; factor completes
;;; term completes
;;; expression completes
	MOV	-(R6), R1	; StatementAssign: 数を取り出して、変数アドレスを取り出し、そのアドレスに値を積む<[24行目,3文字目の'=']>
	MOV	-(R6), R0	; StatementAssign:
	MOV	R1, (R0)	; StatementAssign:
;;; statementAssign completes
;;; statement completes
;;; statement starts
;;; statementAssign starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#b, (R6)+	; Ident: 変数アドレスを積む<[25行目,1文字目の'b']>
;;; ident completes
;;; variable completes
;;; Primary completes
;;; expression starts
;;; term starts
;;; factor starts
;;; unsignedFactor starts
;;; addressToValue starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#b, (R6)+	; Ident: 変数アドレスを積む<[25行目,5文字目の'b']>
;;; ident completes
;;; variable completes
;;; Primary completes
	MOV	-(R6), R0	; AddressToValue: アドレスを取り出して、内容を参照して、積む<>
	MOV	(R0), (R6)+	; AddressToValue:
;;; addressToValue completes
;;; unsignedFactor completes
;;; factor completes
;;; term completes
;;; term starts
;;; factor starts
;;; unsignedFactor starts
;;; addressToValue starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#a, (R6)+	; Ident: 変数アドレスを積む<[25行目,9文字目の'a']>
;;; ident completes
;;; variable completes
;;; Primary completes
	MOV	-(R6), R0	; AddressToValue: アドレスを取り出して、内容を参照して、積む<>
	MOV	(R0), (R6)+	; AddressToValue:
;;; addressToValue completes
;;; unsignedFactor completes
;;; factor completes
;;; term completes
	MOV	-(R6), R0	; ExpressionAdd: ２数を取り出して、足し、積む<[25行目,7文字目の'+']>
	ADD	-(R6), R0	; ExpressionAdd:
	MOV	R0, (R6)+	; ExpressionAdd:
;;; expression completes
	MOV	-(R6), R1	; StatementAssign: 数を取り出して、変数アドレスを取り出し、そのアドレスに値を積む<[25行目,3文字目の'=']>
	MOV	-(R6), R0	; StatementAssign:
	MOV	R1, (R0)	; StatementAssign:
;;; statementAssign completes
;;; statement completes
;;; statement starts
;;; statementAssign starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#a, (R6)+	; Ident: 変数アドレスを積む<[26行目,1文字目の'a']>
;;; ident completes
;;; variable completes
;;; Primary completes
;;; expression starts
;;; term starts
;;; factor starts
;;; unsignedFactor starts
;;; addressToValue starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#a, (R6)+	; Ident: 変数アドレスを積む<[26行目,5文字目の'a']>
;;; ident completes
;;; variable completes
;;; Primary completes
	MOV	-(R6), R0	; AddressToValue: アドレスを取り出して、内容を参照して、積む<>
	MOV	(R0), (R6)+	; AddressToValue:
;;; addressToValue completes
;;; unsignedFactor completes
;;; factor completes
;;; term completes
;;; term starts
;;; factor starts
;;; unsignedFactor starts
;;; number starts
	MOV	#5, (R6)+	; Number: 数を積む<[26行目,9文字目の'5']>
;;; number completes
;;; unsignedFactor completes
;;; factor completes
;;; term completes
	MOV	-(R6), R0	; ExpressionSub: ２数を取り出して、引き、積む<[26行目,7文字目の'-']>
	MOV	-(R6), R1	; ExpressionSub:
	SUB	R0, R1	; ExpressionSub:
	MOV	R1, (R6)+	; ExpressionSub:
;;; expression completes
	MOV	-(R6), R1	; StatementAssign: 数を取り出して、変数アドレスを取り出し、そのアドレスに値を積む<[26行目,3文字目の'=']>
	MOV	-(R6), R0	; StatementAssign:
	MOV	R1, (R0)	; StatementAssign:
;;; statementAssign completes
;;; statement completes
;;; statement starts
;;; statementAssign starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#c, (R6)+	; Ident: 変数アドレスを積む<[27行目,1文字目の'c']>
;;; ident completes
;;; Array starts
;;; expression starts
;;; term starts
;;; factor starts
;;; unsignedFactor starts
;;; addressToValue starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#a, (R6)+	; Ident: 変数アドレスを積む<[27行目,3文字目の'a']>
;;; ident completes
;;; variable completes
;;; Primary completes
	MOV	-(R6), R0	; AddressToValue: アドレスを取り出して、内容を参照して、積む<>
	MOV	(R0), (R6)+	; AddressToValue:
;;; addressToValue completes
;;; unsignedFactor completes
;;; factor completes
;;; term completes
;;; expression completes
	MOV	-(R6), R1	; Array: 数を取り出して、アドレスを取り出して、数をアドレスに足して、積む<[27行目,2文字目の'[']>
	MOV	-(R6), R0	; Array:
	ADD	R1, R0	; Array:
	MOV	R0, (R6)+	; Array:
;;; Array completes
;;; variable completes
;;; Primary completes
;;; expression starts
;;; term starts
;;; factor starts
;;; unsignedFactor starts
;;; addressToValue starts
;;; Primary starts
;;; variable starts
;;; ident starts
	MOV	#e, (R6)+	; Ident: 変数アドレスを積む<[27行目,8文字目の'e']>
;;; ident completes
;;; variable completes
;;; Primary completes
	MOV	-(R6), R0	; AddressToValue: アドレスを取り出して、内容を参照して、積む<>
	MOV	(R0), (R6)+	; AddressToValue:
;;; addressToValue completes
;;; unsignedFactor completes
;;; factor completes
;;; term completes
;;; expression completes
	MOV	-(R6), R1	; StatementAssign: 数を取り出して、変数アドレスを取り出し、そのアドレスに値を積む<[27行目,6文字目の'=']>
	MOV	-(R6), R0	; StatementAssign:
	MOV	R1, (R0)	; StatementAssign:
;;; statementAssign completes
;;; statement completes
	HLT			; ProgramNode:
	.END			; ProgramNode:
;;; program completes