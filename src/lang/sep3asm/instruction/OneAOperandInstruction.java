package lang.sep3asm.instruction;

import lang.sep3asm.Sep3asmParseContext;
import lang.sep3asm.parse.Operand;

public class OneAOperandInstruction extends Sep3Instruction {
    public OneAOperandInstruction(int opCode, int from, int to) {
        super(opCode, from, to);
    }
    public void generate(Sep3asmParseContext ctx, Operand op1, Operand op2) {
        int outCode;
        outCode = opCode;
        outCode |= op2.to5bits();
        ctx.getIOContext().getOutStream().printf("%08X:%04X\n", ctx.getLocationCounter(), outCode);
        ctx.addLocationCounter(1);
    }
}
