package lang.sep3asm.instruction;

import lang.sep3asm.Sep3asmParseContext;
import lang.sep3asm.parse.Operand;

public class OneOperandInstruction extends Sep3Instruction {
    public OneOperandInstruction(int opCode, int from, int to) {
        super(opCode, from, to);
    }
    public void generate(Sep3asmParseContext ctx, Operand op1, Operand op2) {
        int outCode;
        outCode = opCode;
        outCode |= (op1.to5bits() << 5);
        ctx.getIOContext().getOutStream().printf("%08X:%04X\n", ctx.getLocationCounter(), outCode);
        ctx.addLocationCounter(1);
        if(op1.needExtraWord()) {
            outCode = op1.getExtraWord();
            ctx.getIOContext().getOutStream().printf("%08X:%04X\n", ctx.getLocationCounter(), outCode);
            ctx.addLocationCounter(1);
        }
    }
}
