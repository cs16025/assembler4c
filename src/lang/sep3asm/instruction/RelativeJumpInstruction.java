package lang.sep3asm.instruction;

import lang.sep3asm.*;
import lang.sep3asm.parse.Operand;

public class RelativeJumpInstruction extends Sep3Instruction {
	public RelativeJumpInstruction(int opCode, int from, int to) {
		super(opCode, from, to);
	}
	public void generate(Sep3asmParseContext ctx, Operand op1, Operand op2) {
		int outCode;
		outCode = opCode;
		outCode |= (op1.to5bits() << 5);
		ctx.getIOContext().getOutStream().printf("%08X:%04X\n", ctx.getLocationCounter(), outCode);
		ctx.addLocationCounter(1);
		if(op1.needExtraWord()) {
			int immnum;
			immnum = op1.getExtraWord() - ctx.getLocationCounter() - 1;
			outCode = immnum & 0xffff;
			ctx.getIOContext().getOutStream().printf("%08X:%04X\n", ctx.getLocationCounter(), outCode);
			ctx.addLocationCounter(1);
		}
	}
}
