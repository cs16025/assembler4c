package lang.sep3asm;

import java.util.HashMap;

import lang.sep3asm.instruction.*;
import lang.sep3asm.parse.Operand;

public class Sep3asmTokenRule extends HashMap<String, Object> {
	private static final long serialVersionUID = 6928902799089728690L;
	private static final int D		= Operand.REGISTER;		// レジスタアドレシングを許す
	private static final int I		= Operand.INDIRECT;		// レジスタ間接アドレシングを許す
	private static final int MI		= Operand.PREDEC;		// プレデクリメント・レジスタ間接アドレシングを許す
	private static final int IP		= Operand.POSTINC;		// ポストインクリメント・レジスタ間接アドレシングを許す
	private static final int IMM	= Operand.IMM;			// 即値（イミディエイト）を許す
	private static final int LABEL	= Operand.LABEL;		// 解析時のみ、ラベルを＃なしに書いてある

	// toオペランドに -(R?)を使うのは禁止
	// fromオペランドに -(R?) を使うときは、R6,SPに限定される
	// toオペランドに (R7), (R7)+ を使うのは禁止

	public Sep3asmTokenRule() {
        put(".word", new TokenAssoc(Sep3asmToken.TK_DOTWD, null));
		put("-",  new TokenAssoc(Sep3asmToken.TK_MINUS, null));
        put(",", new TokenAssoc(Sep3asmToken.TK_COMMA, null));                                                                         //第九章にて追加
        put("\n", new TokenAssoc(Sep3asmToken.TK_NL, null));                                                                           //第九章にて追加

		put("hlt", new TokenAssoc(Sep3asmToken.TK_INST0, new ZeroOperandInstruction(0x0000, 0, 0)));
		put("clr", new TokenAssoc(Sep3asmToken.TK_INST1a, new OneAOperandInstruction(0x1000, 0, D|I|IP)));               //第九章にて追加
        put("asl", new TokenAssoc(Sep3asmToken.TK_INST1a, new OneAOperandInstruction(0x2000, 0, D|I|IP)));               //第九章にて追加
        put("asr", new TokenAssoc(Sep3asmToken.TK_INST1a, new OneAOperandInstruction(0x2400, 0, D|I|IP)));               //第九章にて追加
        put("lsl", new TokenAssoc(Sep3asmToken.TK_INST1a, new OneAOperandInstruction(0x3000, 0, D|I|IP)));               //第九章にて追加
        put("lsr", new TokenAssoc(Sep3asmToken.TK_INST1a, new OneAOperandInstruction(0x3400, 0, D|I|IP)));               //第九章にて追加
        put("rol", new TokenAssoc(Sep3asmToken.TK_INST1a, new OneAOperandInstruction(0x3800, 0, D|I|IP)));               //第九章にて追加
        put("ror", new TokenAssoc(Sep3asmToken.TK_INST1a, new OneAOperandInstruction(0x3C00, 0, D|I|IP)));               //第九章にて追加
        put("mov", new TokenAssoc(Sep3asmToken.TK_INST2, new TwoOperandInstruction(0x4000, D|I|MI|IP|IMM, D|I|IP)));
        put("jmp", new TokenAssoc(Sep3asmToken.TK_INST1, new OneOperandInstruction(0x4407, D|I|MI|IP|IMM|LABEL, 0)));    //第九章にて追加
        put("ret", new TokenAssoc(Sep3asmToken.TK_INST0, new ZeroOperandInstruction(0x4AC7, 0, 0)));                     //第九章にて追加
        put("rit", new TokenAssoc(Sep3asmToken.TK_INST0, new ZeroOperandInstruction(0x4AC7, 0, 0)));                     //第九章にて追加
        put("add", new TokenAssoc(Sep3asmToken.TK_INST2, new TwoOperandInstruction(0x5000, D|I|MI|IP|IMM, D|I|IP)));     //第九章にて追加
        put("rjp", new TokenAssoc(Sep3asmToken.TK_INST1, new RelativeJumpInstruction(0x5403, D|I|MI|IP|IMM|LABEL, 0)));  //第九章にて追加
        put("sub", new TokenAssoc(Sep3asmToken.TK_INST2, new TwoOperandInstruction(0x6000, D|I|MI|IP|IMM, D|I|IP)));     //第九章にて追加
        put("cmp", new TokenAssoc(Sep3asmToken.TK_INST2, new TwoOperandInstruction(0x6C00, D|I|MI|IP|IMM, D|I|IP)));     //第九章にて追加
        put("nop", new TokenAssoc(Sep3asmToken.TK_INST0, new ZeroOperandInstruction(0x7000, 0, 0)));                     //第九章にて追加
        put("or", new TokenAssoc(Sep3asmToken.TK_INST2, new TwoOperandInstruction(0x8000, D|I|MI|IP|IMM, D|I|IP)));      //第九章にて追加
        put("xor", new TokenAssoc(Sep3asmToken.TK_INST2, new TwoOperandInstruction(0x8400, D|I|MI|IP|IMM, D|I|IP)));     //第九章にて追加
        put("and", new TokenAssoc(Sep3asmToken.TK_INST2, new TwoOperandInstruction(0x8800, D|I|MI|IP|IMM, D|I|IP)));     //第九章にて追加
        put("bit", new TokenAssoc(Sep3asmToken.TK_INST2, new TwoOperandInstruction(0x8C00, D|I|MI|IP|IMM, D|I|IP)));     //第九章にて追加
        put("jsr", new TokenAssoc(Sep3asmToken.TK_INST1, new OneOperandInstruction(0xB01E, D|I|IP|IMM|LABEL, 0)));       //第九章にて追加
        put("rjs", new TokenAssoc(Sep3asmToken.TK_INST1, new RelativeJumpInstruction(0xB41E, D|I|MI|IP|IMM|LABEL, 0)));
        put("svc", new TokenAssoc(Sep3asmToken.TK_INST1, new OneOperandInstruction(0xB81E, D|I|IP|IMM|LABEL, 0)));       //第九章にて追加
        put("brn", new TokenAssoc(Sep3asmToken.TK_INST1, new OneOperandInstruction(0xC007, D|I|IP|IMM|LABEL, 0)));       //第九章にて追加
        put("brz", new TokenAssoc(Sep3asmToken.TK_INST1, new OneOperandInstruction(0xC407, D|I|IP|IMM|LABEL, 0)));       //第九章にて追加
        put("brv", new TokenAssoc(Sep3asmToken.TK_INST1, new OneOperandInstruction(0xC807, D|I|IP|IMM|LABEL, 0)));       //第九章にて追加
        put("brc", new TokenAssoc(Sep3asmToken.TK_INST1, new OneOperandInstruction(0xCC07, D|I|IP|IMM|LABEL, 0)));       //第九章にて追加
        put("rbn", new TokenAssoc(Sep3asmToken.TK_INST1, new RelativeJumpInstruction(0xE007, D|I|IP|IMM|LABEL, 0)));     //第九章にて追加
        put("rbz", new TokenAssoc(Sep3asmToken.TK_INST1, new RelativeJumpInstruction(0xE407, D|I|IP|IMM|LABEL, 0)));     //第九章にて追加
        put("rbv", new TokenAssoc(Sep3asmToken.TK_INST1, new RelativeJumpInstruction(0xE807, D|I|IP|IMM|LABEL, 0)));     //第九章にて追加
        put("rbc", new TokenAssoc(Sep3asmToken.TK_INST1, new RelativeJumpInstruction(0xEC07, D|I|IP|IMM|LABEL, 0)));     //第九章にて追加

        put("+", new TokenAssoc(Sep3asmToken.TK_PLUS, null));                                                                          //第九章にて追加
		put(".", new TokenAssoc(Sep3asmToken.TK_DOT, null));                                                                           //第九章にて追加
		put("=", new TokenAssoc(Sep3asmToken.TK_EQUAL, null));                                                                         //第九章にて追加
        put(".blkw", new TokenAssoc(Sep3asmToken.TK_DOTBLKW, null));                                                                   //第九章にて追加
        put(":", new TokenAssoc(Sep3asmToken.TK_COLON, null));                                                                         //第九章にて追加
        put("#", new TokenAssoc(Sep3asmToken.TK_SHARP, null));                                                                         //第九章にて追加
        put("(", new TokenAssoc(Sep3asmToken.TK_LBRACKET, null));                                                                      //第九章にて追加
        put(")", new TokenAssoc(Sep3asmToken.TK_RBRACKET, null));                                                                      //第九章にて追加
        put("r0", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(0)));                                                               //第九章にて追加
        put("r1", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(1)));                                                               //第九章にて追加
        put("r2", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(2)));                                                               //第九章にて追加
        put("r3", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(3)));                                                               //第九章にて追加
        put("r4", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(4)));                                                               //第九章にて追加
        put("r5", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(5)));                                                               //第九章にて追加
        put("r6", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(6)));                                                               //第九章にて追加
        put("r7", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(7)));                                                               //第九章にて追加
        put("psw", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(5)));                                                              //第九章にて追加
        put("sp", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(6)));                                                               //第九章にて追加
        put("pc", new TokenAssoc(Sep3asmToken.TK_REG, new Integer(7)));                                                               //第九章にて追加
        put("\t", new TokenAssoc(Sep3asmToken.TK_TAB, null));                                                                          //第九章にて追加
        put(";", new TokenAssoc(Sep3asmToken.TK_SEMICOLON, null));                                                                     //第九章にて追加
        put(".end", new TokenAssoc(Sep3asmToken.TK_DOTEND, null));                                                                     //第九章にて追加
        put(".=", new TokenAssoc(Sep3asmToken.TK_DOTEQUAL, null));                                                                     //第九章にて追加
	}
}
