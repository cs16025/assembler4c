package lang.sep3asm;

import lang.SymbolTable;

import java.util.HashSet;

public class Sep3asmSymbolTable extends SymbolTable<LabelEntry> {
	private static final long serialVersionUID = 2434424863156998459L;

	@Override
	public LabelEntry register(String name, LabelEntry e) {
		return put(name, e);
	}

	@Override
	public LabelEntry search(String name) {
		return null;
	}

	public Integer resolve(String label) {
        HashSet<LabelEntry> visitedSet = new HashSet<>();
        LabelEntry d = get(label);
        while ((d != null) && d.isLabel()) {
            if (visitedSet.contains(d)) {
                return null;
            }
            visitedSet.add(d);
            d = get(d.getLabel());
        }
        if ((d != null) && d.isInteger()) {
            for (LabelEntry labelEntry : visitedSet) {
                labelEntry.setInteger(d.getInteger());
            }
        }
        return (d == null) ? null : d.getInteger();
    }
}
